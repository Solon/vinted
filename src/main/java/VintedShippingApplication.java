import controller.ShipmentController;

public class VintedShippingApplication {

    public static void main(String[] args) {
        final ShipmentController shipmentController = new ShipmentController();
        shipmentController.handleShipments();
    }
}
