package domain;

public enum PackageSize {
    L,
    M,
    S
}