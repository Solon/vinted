package domain;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Shipment {
    private LocalDate date;
    private PackageSize packageSize;
    private Provider provider;
    private float price;
    private Float discountAmount;
    private Float discountedPrice;
    private boolean isValid;
    private String invalidShipmentParseText;

    // primary constructor
    public Shipment(final LocalDate date, final PackageSize packageSize, final Provider provider) {
        this.date = date;
        this.packageSize = packageSize;
        this.provider = provider;
        this.isValid = true;
    }

    // for creating invalid shipments
    public Shipment(final String parseText) {
        this.isValid = false;
        this.invalidShipmentParseText = parseText;
    }

    public LocalDate getDate() {
        return date;
    }

    public PackageSize getPackageSize() {
        return packageSize;
    }

    public Provider getProvider() {
        return provider;
    }

    public float getPrice() {
        return price;
    }

    public Float getDiscountAmount() {
        return discountAmount;
    }

    public Float getDiscountedPrice() {
        return discountedPrice;
    }

    public float getShipmentPrice() {
        if (discountAmount != null) {
            return discountedPrice;
        } else {
            return price;
        }
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setDiscountAmount(final float discountAmount) {
        this.discountAmount = discountAmount;
        calculateDiscountedPrice();
    }

    private void calculateDiscountedPrice() {
        final BigDecimal precisePrice = new BigDecimal(getPrice());
        final BigDecimal preciseDiscountAmount = new BigDecimal(getDiscountAmount());
        discountedPrice = precisePrice.subtract(preciseDiscountAmount).floatValue();
    }

    public boolean isValid() {
        return isValid;
    }

    public String getInvalidShipmentParseText() {
        return invalidShipmentParseText;
    }
}
