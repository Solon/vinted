package domain.discounts;

import domain.Shipment;

public interface Discount {
    Float calculateDiscount(Shipment shipment);
}
