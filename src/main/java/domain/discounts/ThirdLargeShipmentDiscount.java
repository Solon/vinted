package domain.discounts;

import domain.PackageSize;
import domain.Provider;
import domain.Shipment;
import service.ShipmentBasePriceCalculator;

import java.util.HashMap;
import java.util.Map;

/**
 * Third L shipment via LP should be free, but only once a calendar month.
 */
public class ThirdLargeShipmentDiscount implements Discount {
    private Map<String, Integer> lpLargeMonthlyShipments;

    public ThirdLargeShipmentDiscount() {
        lpLargeMonthlyShipments = new HashMap<>();
    }

    /**
     * @param shipment
     * @return discount amount or null if discount is not applicable
     */
    @Override
    public Float calculateDiscount(final Shipment shipment) {
        if (shipment.getProvider() == Provider.LP && shipment.getPackageSize() == PackageSize.L) {
            final String monthDate = "" + shipment.getDate().getYear() + shipment.getDate().getMonthValue();
            if (lpLargeMonthlyShipments.get(monthDate) == null) {
                lpLargeMonthlyShipments.put(monthDate, 1);
            } else {
                lpLargeMonthlyShipments.merge(monthDate, 1, Integer::sum);
            }
            if (lpLargeMonthlyShipments.get(monthDate) == 3) {
                return ShipmentBasePriceCalculator.LP_L_PRICE;
            }
        }
        return null;
    }
}
