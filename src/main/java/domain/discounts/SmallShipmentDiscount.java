package domain.discounts;

import domain.PackageSize;
import domain.Shipment;
import service.ShipmentBasePriceCalculator;

import java.math.BigDecimal;

/**
 * All S shipments should always match lowest S package price among the providers.
 */
public class SmallShipmentDiscount implements Discount {

    public SmallShipmentDiscount() {
    }

    /**
     * @param shipment
     * @return discount amount or null if discount is not applicable
     */
    @Override
    public Float calculateDiscount(final Shipment shipment) {
        if (shipment.getPackageSize() == PackageSize.S) {
            final Float baseShipmentPrice = ShipmentBasePriceCalculator.getShipmentPrice(shipment);
            if (baseShipmentPrice != null) {
                final float discountShipmentPrice = Math.min(ShipmentBasePriceCalculator.LP_S_PRICE, ShipmentBasePriceCalculator.MR_S_PRICE);
                final float discountAmount = new BigDecimal(baseShipmentPrice).subtract(new BigDecimal(discountShipmentPrice)).floatValue();
                // return null if amount is equal to 0, indicating that account is not applicable
                return (discountAmount > 0) ? discountAmount : null;
            } else {
                // TODO handle error properly
                System.out.println("Error: base shipment price not found for shipment " + shipment.getDate());
                return null;
            }
        } else {
            return null;
        }
    }
}
