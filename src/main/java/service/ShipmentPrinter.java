package service;

import domain.Shipment;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

/**
 * Service to print out shipments and applied discounts for them
 */
public class ShipmentPrinter {
    public ShipmentPrinter() {
    }

    public void printShipments(final List<Shipment> shipments) {
        for (Shipment shipment : shipments) {
            if (shipment.isValid()) {
                final DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ENGLISH);
                // XXX: without the locale change it uses commas instead of points for decimal separators (lithuanian locale)
                decimalFormat.applyPattern("#,##0.00");
                final String discountOutput = (shipment.getDiscountAmount() == null) ? "-" : decimalFormat.format(shipment.getDiscountAmount());
                System.out.println(shipment.getDate().toString()
                        + " " + shipment.getPackageSize().toString()
                        + " " + shipment.getProvider().toString()
                        + " " + decimalFormat.format(shipment.getShipmentPrice())
                        + " " + discountOutput);
            } else {
                System.out.println(shipment.getInvalidShipmentParseText() + " Ignored");
            }
        }
    }
}
