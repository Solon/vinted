package service;

import domain.Shipment;
import domain.discounts.SmallShipmentDiscount;
import domain.discounts.ThirdLargeShipmentDiscount;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service to calculate and apply shipment discounts
 */
public class ShipmentDiscountCalculator {
    private SmallShipmentDiscount smallShipmentDiscount;
    private ThirdLargeShipmentDiscount thirdLargeShipmentDiscount;
    private final static BigDecimal MAX_MONTHLY_DISCOUNT_ALLOWANCE = new BigDecimal(10);
    private Map<String, Float> monthlyDiscounts;

    public ShipmentDiscountCalculator() {
        smallShipmentDiscount = new SmallShipmentDiscount();
        thirdLargeShipmentDiscount = new ThirdLargeShipmentDiscount();
        monthlyDiscounts = new HashMap<>();
    }

    public void applyDiscounts(final List<Shipment> shipments) {
        for (Shipment shipment : shipments) {
            if (shipment.isValid()) {
                Float discountAmount;
                discountAmount = smallShipmentDiscount.calculateDiscount(shipment);
                if (discountAmount != null) {
                    applyDiscount(shipment, discountAmount);
                }
                discountAmount = thirdLargeShipmentDiscount.calculateDiscount(shipment);
                if (discountAmount != null) {
                    applyDiscount(shipment, discountAmount);
                }
                //TODO clarify and implement business case where more than one discount applies to a shipment
            }
        }
    }

    /**
     * Apply discounts taking into account max monthly amount
     */
    private void applyDiscount(final Shipment shipment, final Float discountAmount) {
        final String monthDate = "" + shipment.getDate().getYear() + shipment.getDate().getMonthValue();
        // already used discount amount for this month
        final Float appliedMonthlyDiscount = monthlyDiscounts.get(monthDate);
        if (appliedMonthlyDiscount != null) {
            BigDecimal monthlyRemainder = MAX_MONTHLY_DISCOUNT_ALLOWANCE.subtract(new BigDecimal(appliedMonthlyDiscount));
            BigDecimal discountAmountDecimal = new BigDecimal(discountAmount);
            // if monthy allowance remainder is less than discount amount
            if (monthlyRemainder.compareTo(discountAmountDecimal) == -1) {
                // apply only the remainder amount
                shipment.setDiscountAmount(monthlyRemainder.floatValue());
                monthlyDiscounts.put(monthDate, MAX_MONTHLY_DISCOUNT_ALLOWANCE.floatValue());
            } else {
                // apply full discount amount
                shipment.setDiscountAmount(discountAmount);
                Float newAppliedMonthyDiscount = new BigDecimal(appliedMonthlyDiscount).add(discountAmountDecimal).floatValue();
                monthlyDiscounts.put(monthDate, newAppliedMonthyDiscount);
            }
        } else {
            shipment.setDiscountAmount(discountAmount);
            monthlyDiscounts.put(monthDate, discountAmount);
        }
    }
}
