package service;

import domain.PackageSize;
import domain.Provider;
import domain.Shipment;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Service to read and parse shipments information from a file
 */
public class ShipmentFileParser {
    private static final String FILE_PATH = "src/main/resources/input.txt";
    private List<Shipment> shipments;

    public ShipmentFileParser() {
        shipments = new ArrayList<>();
    }

    public List<Shipment> readShipments() {
        final BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(
                    FILE_PATH));
            String line = reader.readLine();
            while (line != null) {
                try {
                    final int year = Integer.parseInt(line.substring(0, 4));
                    final int month = Integer.parseInt(line.substring(5, 7));
                    final int day = Integer.parseInt(line.substring(8, 10));
                    final String sizeString = line.substring(11, 12);
                    final String packageString = line.substring(13, 15);
                    shipments.add(new Shipment(LocalDate.of(year, month, day), PackageSize.valueOf(sizeString), Provider.valueOf(packageString)));
                } catch (RuntimeException ex) {
                    shipments.add(new Shipment(line));
                }
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return shipments;
    }
}
