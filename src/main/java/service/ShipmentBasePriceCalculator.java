package service;

import domain.Shipment;

import java.util.List;

/**
 * Service to calculate and apply base shipment prices
 */
public class ShipmentBasePriceCalculator {
    public static final float LP_S_PRICE = 1.50f;
    public static final float LP_M_PRICE = 4.90f;
    public static final float LP_L_PRICE = 6.90f;
    public static final float MR_S_PRICE = 2.00f;
    public static final float MR_M_PRICE = 3.00f;
    public static final float MR_L_PRICE = 4.00f;

    public ShipmentBasePriceCalculator() {
    }

    public void applyPrices(final List<Shipment> shipments) {
        for (Shipment shipment : shipments) {
            if (shipment.isValid()) {
                final Float shipmentPrice = getShipmentPrice(shipment);
                if (shipmentPrice != null) {
                    shipment.setPrice(shipmentPrice);
                } else {
                    // TODO handle error properly
                    System.out.println("Error: base shipment price not found for shipment " + shipment.getDate());
                }
            }
        }
    }

    public static Float getShipmentPrice(final Shipment shipment) {
        switch (shipment.getProvider()) {
            case LP:
                switch (shipment.getPackageSize()) {
                    case S:
                        return LP_S_PRICE;
                    case M:
                        return LP_M_PRICE;
                    case L:
                        return LP_L_PRICE;
                }
                break;
            case MR:
                switch (shipment.getPackageSize()) {
                    case S:
                        return MR_S_PRICE;
                    case M:
                        return MR_M_PRICE;
                    case L:
                        return MR_L_PRICE;
                }
                break;
        }

        return null;
    }
}
