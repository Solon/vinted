package controller;

import domain.Shipment;
import service.ShipmentBasePriceCalculator;
import service.ShipmentDiscountCalculator;
import service.ShipmentFileParser;
import service.ShipmentPrinter;

import java.util.List;

public class ShipmentController {
    private ShipmentFileParser fileReader;
    private ShipmentBasePriceCalculator basePriceCalculator;
    private ShipmentDiscountCalculator discountCalculator;
    private ShipmentPrinter shipmentPrinter;

    public ShipmentController() {
        fileReader = new ShipmentFileParser();
        basePriceCalculator = new ShipmentBasePriceCalculator();
        discountCalculator = new ShipmentDiscountCalculator();
        shipmentPrinter = new ShipmentPrinter();
    }

    public void handleShipments() {
        final List<Shipment> shipments = fileReader.readShipments();
        basePriceCalculator.applyPrices(shipments);
        discountCalculator.applyDiscounts(shipments);
        shipmentPrinter.printShipments(shipments);
    }
}
