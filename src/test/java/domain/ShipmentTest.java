package domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShipmentTest {
    private Shipment shipment;

    @BeforeEach
    void setup() {
        shipment = new Shipment(LocalDate.of(1999, 1, 13), PackageSize.L, Provider.LP);
    }

    @Test
    public void setDiscountAmount_discountAmount_calculatedDiscountedPrice() {
        shipment.setPrice(6.9f);
        shipment.setDiscountAmount(0.5f);
        assertEquals(0, new BigDecimal(shipment.getShipmentPrice()).compareTo(new BigDecimal(6.4f)));
    }
}
