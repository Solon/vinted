package domain.discounts;

import domain.PackageSize;
import domain.Provider;
import domain.Shipment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class SmallShipmentDiscountTest {
    private Shipment largeShipment;
    private Shipment smallLPShipment;
    private Shipment smallMRShipment;
    private final SmallShipmentDiscount smallShipmentDiscount = new SmallShipmentDiscount();

    @BeforeEach
    void setup() {
        largeShipment = new Shipment(LocalDate.of(1999, 1, 13), PackageSize.L, Provider.LP);
        smallLPShipment = new Shipment(LocalDate.of(1999, 1, 13), PackageSize.S, Provider.LP);
        smallMRShipment = new Shipment(LocalDate.of(1999, 1, 13), PackageSize.S, Provider.MR);
    }

    @Test
    public void calculateDiscount_notS_returnNull() {
        assertNull(smallShipmentDiscount.calculateDiscount(largeShipment));
    }

    @Test
    public void calculateDiscount_priceLowest_returnNull() {
        // LP Small shipment is cheapest
        assertNull(smallShipmentDiscount.calculateDiscount(smallLPShipment));
    }

    @Test
    public void calculateDiscount_priceHigher_returnCorrectDiscount() {
        // MR Small shipment is more expensive than LP by 0.50
        assertEquals(0.50f, (float) smallShipmentDiscount.calculateDiscount(smallMRShipment));
    }
}
