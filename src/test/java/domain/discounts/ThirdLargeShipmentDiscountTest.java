package domain.discounts;

import domain.PackageSize;
import domain.Provider;
import domain.Shipment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ThirdLargeShipmentDiscountTest {
    private Shipment largeLPShipment;
    private Shipment largeMRShipment;
    private Shipment smallShipment;
    private final ThirdLargeShipmentDiscount thirdLargeShipmentDiscount = new ThirdLargeShipmentDiscount();

    @BeforeEach
    void setup() {
        largeLPShipment = new Shipment(LocalDate.of(1999, 1, 13), PackageSize.L, Provider.LP);
        largeMRShipment = new Shipment(LocalDate.of(1999, 1, 13), PackageSize.L, Provider.MR);
        smallShipment = new Shipment(LocalDate.of(1999, 1, 13), PackageSize.S, Provider.LP);
    }

    @Test
    public void calculateDiscount_notL_returnNull() {
        assertNull(thirdLargeShipmentDiscount.calculateDiscount(smallShipment));
    }

    @Test
    public void calculateDiscount_notLP_returnNull() {
        assertNull(thirdLargeShipmentDiscount.calculateDiscount(largeMRShipment));
    }

    @Test
    public void calculateDiscount_largeLP_returnCorrectDiscountOnThird() {
        // LP Small shipment is cheapest
        assertNull(thirdLargeShipmentDiscount.calculateDiscount(largeLPShipment));
        assertNull(thirdLargeShipmentDiscount.calculateDiscount(largeLPShipment));
        assertEquals(6.90f, (float) thirdLargeShipmentDiscount.calculateDiscount(largeLPShipment));
        assertNull(thirdLargeShipmentDiscount.calculateDiscount(largeLPShipment));
    }

}
