package service;

import domain.PackageSize;
import domain.Shipment;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ShipmentFileParserTest {
    private final ShipmentFileParser shipmentFileParser = new ShipmentFileParser();

    @Test
    public void readShipments_validInput_shipmentsCreated() {
        // make private field readable
        try {
            Field field = ShipmentFileParser.class.getDeclaredField("FILE_PATH");
            field.setAccessible(true);
            field.set(field, "src/test/resources/valid_input.txt");
            List<Shipment> shipments = shipmentFileParser.readShipments();
            assertFalse(shipments.isEmpty());
            Shipment shipment = shipments.get(0);
            assertNotNull(shipment);
            assertEquals(PackageSize.S, shipment.getPackageSize());
        } catch (NoSuchFieldException | IllegalAccessException ex) {
            // test not possible
        }

    }

    @Test
    public void readShipments_invalidInput_invalidShipmentsCreated() {
        // make private field readable
        try {
            Field field = ShipmentFileParser.class.getDeclaredField("FILE_PATH");
            field.setAccessible(true);
            field.set(field, "src/test/resources/invalid_input.txt");
            List<Shipment> shipments = shipmentFileParser.readShipments();
            assertFalse(shipments.isEmpty());
            Shipment shipment = shipments.get(0);
            assertNotNull(shipment);
            assertNotEquals(PackageSize.S, shipment.getPackageSize());
            assertEquals("2015-02-01 S MRFDSDFSD", shipment.getInvalidShipmentParseText());
        } catch (NoSuchFieldException | IllegalAccessException ex) {
            // test not possible
        }

    }

}
