package service;

import domain.PackageSize;
import domain.Provider;
import domain.Shipment;
import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ShipmentPrinterTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final ShipmentPrinter shipmentPrinter = new ShipmentPrinter();
    private List<Shipment> shipments;

    @BeforeAll
    void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterAll
    void restoreStreams() {
        System.setOut(originalOut);
    }

    @BeforeEach
    void setup() {
        shipments = new ArrayList<>();
    }

    @Test
    public void printShipments_shipments_systemOutput() {
        final Shipment shipment = new Shipment(LocalDate.of(2015, 2, 1), PackageSize.S, Provider.MR);
        shipment.setPrice(2.00f);
        shipment.setDiscountAmount(0.50f);
        shipments.add(shipment);
        shipmentPrinter.printShipments(shipments);
        assertEquals("2015-02-01 S MR 1.50 0.50" + System.getProperty("line.separator"), outContent.toString());
    }
}
