package service;

import domain.PackageSize;
import domain.Provider;
import domain.Shipment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ShipmentDiscountCalculatorTest {
    private Shipment validShipment1;
    private Shipment validShipment2;
    private Shipment validShipment3;
    private Shipment invalidShipment;
    private List<Shipment> shipments;
    private final ShipmentDiscountCalculator shipmentDiscountCalculator = new ShipmentDiscountCalculator();

    @BeforeEach
    void setup() {
        shipments = new ArrayList<>();
        validShipment1 = new Shipment(LocalDate.of(1999, 1, 13), PackageSize.L, Provider.LP);
        validShipment1.setPrice(6.90f);
        validShipment2 = new Shipment(LocalDate.of(1999, 1, 13), PackageSize.S, Provider.MR);
        validShipment2.setPrice(2.00f);
        validShipment3 = new Shipment(LocalDate.of(1999, 2, 13), PackageSize.S, Provider.MR);
        validShipment2.setPrice(2.00f);
        invalidShipment = new Shipment("This is invalid shipment");
    }

    @Test
    public void applyDiscounts_validShipment_discountApplied() {
        shipments.add(validShipment1);
        shipments.add(validShipment2);
        shipmentDiscountCalculator.applyDiscounts(shipments);
        // no discount for first LP large shipment
        assertNull(validShipment1.getDiscountAmount());
        // 0.50 discount for MR small shipment
        assertEquals(0.50f, (float) validShipment2.getDiscountAmount());
    }

    @Test
    public void applyPrices_invalidShipment_skipped() {
        shipments.add(invalidShipment);
        // testing no exception thrown
        shipmentDiscountCalculator.applyDiscounts(shipments);
        // TODO verify shipment.setPrice was not called with mocks
    }

    @Test
    public void applyDiscount_remainderLessThanDiscount_remainderApplied() {
        try {
            // changing access for the private method
            Method applyDiscountMethod = ShipmentDiscountCalculator.class.getDeclaredMethod("applyDiscount", Shipment.class, Float.class);
            applyDiscountMethod.setAccessible(true);
            applyDiscountMethod.invoke(shipmentDiscountCalculator, validShipment1, 9.00f);
            assertEquals(9.00f, (float) validShipment1.getDiscountAmount());
            applyDiscountMethod.invoke(shipmentDiscountCalculator, validShipment2, 3.00f);
            assertEquals(1.00f, (float) validShipment2.getDiscountAmount());
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException ex) {
            // test not possible
        }
    }

    @Test
    public void applyDiscount_remainderMoreThanDiscount_fullDiscountApplied() {
        try {
            // changing access for the private method
            Method applyDiscountMethod = ShipmentDiscountCalculator.class.getDeclaredMethod("applyDiscount", Shipment.class, Float.class);
            applyDiscountMethod.setAccessible(true);
            applyDiscountMethod.invoke(shipmentDiscountCalculator, validShipment1, 5.00f);
            assertEquals(5.00f, (float) validShipment1.getDiscountAmount());
            applyDiscountMethod.invoke(shipmentDiscountCalculator, validShipment2, 3.00f);
            assertEquals(3.00f, (float) validShipment2.getDiscountAmount());
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException ex) {
            // test not possible
        }
    }

    @Test
    public void applyDiscount_nextMonth_() {
        try {
            // changing access for the private method
            Method applyDiscountMethod = ShipmentDiscountCalculator.class.getDeclaredMethod("applyDiscount", Shipment.class, Float.class);
            applyDiscountMethod.setAccessible(true);
            applyDiscountMethod.invoke(shipmentDiscountCalculator, validShipment1, 9.00f);
            assertEquals(9.00f, (float) validShipment1.getDiscountAmount());
            // different month shipment
            applyDiscountMethod.invoke(shipmentDiscountCalculator, validShipment3, 3.00f);
            assertEquals(3.00f, (float) validShipment3.getDiscountAmount());
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException ex) {
            // test not possible
        }
    }

}
