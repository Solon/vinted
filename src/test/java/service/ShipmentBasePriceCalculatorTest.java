package service;

import domain.PackageSize;
import domain.Provider;
import domain.Shipment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShipmentBasePriceCalculatorTest {
    private Shipment validShipment1;
    private Shipment validShipment2;
    private Shipment invalidShipment;
    private List<Shipment> shipments;
    private final ShipmentBasePriceCalculator shipmentBasePriceCalculator = new ShipmentBasePriceCalculator();

    @BeforeEach
    void setup() {
        shipments = new ArrayList<>();
        validShipment1 = new Shipment(LocalDate.of(1999, 1, 13), PackageSize.L, Provider.LP);
        validShipment2 = new Shipment(LocalDate.of(1999, 1, 13), PackageSize.S, Provider.MR);
        invalidShipment = new Shipment("This is invalid shipment");
    }

    @Test
    public void applyPrices_validShipments_pricesSet() {
        shipments.add(validShipment1);
        shipments.add(validShipment2);
        shipmentBasePriceCalculator.applyPrices(shipments);
        assertEquals(6.90f, validShipment1.getPrice());
        assertEquals(2.00f, validShipment2.getPrice());
    }

    @Test
    public void applyPrices_invalidShipment_skipped() {
        shipments.add(invalidShipment);
        // testing no exception thrown
        shipmentBasePriceCalculator.applyPrices(shipments);
        // TODO verify shipment.setPrice was not called with mocks
    }
}
